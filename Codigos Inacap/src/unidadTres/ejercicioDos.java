package unidadTres;
import javax.swing.*;
public class ejercicioDos {
    public static void main(String args[]){
        int a=0, b=0, sum, rest, mult;
        float div=0f;
        int op = 0;
        do {
            op= Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese numero de la Operacion que desea realizar: \n " +
                    "1.- Suma \n " +
                    "2.- Resta \n " +
                    "3.- Multiplicacion \n " +
                    "4.- Division \n " +
                    "5.- Obtener mayor \n " +
                    "6.- Obtener menor \n " +
                    "7.- Salir \n" ));
            switch (op){
                case 1:
                    a=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Primer Número: ","Suma",JOptionPane.PLAIN_MESSAGE));
                    b=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Segundo Número: ","Suma",JOptionPane.PLAIN_MESSAGE));
                    sum=a+b;
                    JOptionPane.showMessageDialog(null,"El resultado es: "+ sum ,"Suma", JOptionPane.PLAIN_MESSAGE);
                    break;
                case 2:
                    a=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Primer Número: ", "Resta", JOptionPane.PLAIN_MESSAGE));
                    b=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Segundo Número: ", "Resta", JOptionPane.PLAIN_MESSAGE));
                    rest=a-b;
                    JOptionPane.showMessageDialog(null,"El resultado es: "+rest, "Resta", JOptionPane.PLAIN_MESSAGE);
                    break;
                case 3:
                    a=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Primer Número: ", "Multiplicación",JOptionPane.PLAIN_MESSAGE));
                    b=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Segundo Número: ", "Multiplicación",JOptionPane.PLAIN_MESSAGE));
                    mult=a*b;
                    JOptionPane.showMessageDialog(null,"El resultado es: "+mult, "Multiplicación", JOptionPane.PLAIN_MESSAGE);
                    break;
                case 4:
                    a=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Primer Número: ", "División",JOptionPane.PLAIN_MESSAGE));
                    b=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Segundo Número: ", "División",JOptionPane.PLAIN_MESSAGE));
                    div=a/b;
                    JOptionPane.showMessageDialog(null,"El resultado es: " + div, "División", JOptionPane.PLAIN_MESSAGE);
                    break;
                case 5:
                    a=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Primer Número: ", null,JOptionPane.PLAIN_MESSAGE));
                    b=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Segundo Número: ", null,JOptionPane.PLAIN_MESSAGE));
                    if (a>b){
                        JOptionPane.showMessageDialog(null,"El mayor número es: "+a);
                    }else if (b>a){
                        JOptionPane.showMessageDialog(null,"El mayor número es: "+b);
                    }else
                        JOptionPane.showMessageDialog(null,"Los números son iguales.");
                    break;
                case 6:
                    a=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Primer Número: ", null,JOptionPane.PLAIN_MESSAGE));
                    b=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Segundo Número: ", null,JOptionPane.PLAIN_MESSAGE));
                    if (a<b) {
                        JOptionPane.showMessageDialog(null,"El menor número es: "+a);
                    }else if (b<a){
                        JOptionPane.showMessageDialog(null,"El menor número es: "+b);
                    }else
                        JOptionPane.showMessageDialog(null,"Los números son iguales.");
                    break;
            }
        }while(op!=7);

    }
}